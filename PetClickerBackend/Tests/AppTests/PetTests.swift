//
//  PetTests.swift
//
//
//  Created by Ataias Pereira Reis on 19/01/22.
//

import Api
import Fluent
import Spec

@testable import App

final class PetTests: AppTestCase {
  func testCreatePetSucceeds() throws {
    let newPet = exampleNewPet
    var total = 1
    try app
      .describe("Get original pets count")
      .get("/pets")
      .expect(.ok)
      .expect(.json)
      .expect(Page<Pet.Get>.self) { content in
        total += content.metadata.total
      }
      .test()

    try app
      .describe("Creating a Pet should return ok and have same values as given")
      .post("/pets")
      .body(newPet)
      .expect(.created)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, newPet.race)
        XCTAssertEqual(content.breed, newPet.breed)
        XCTAssertEqual(content.urls, newPet.urls)
        XCTAssertEqual(content.weight, newPet.weight)
        XCTAssertEqual(content.height, newPet.height)
        XCTAssertEqual(content.lifeSpan, newPet.lifeSpan)
      }
      .test()

    try app
      .describe("Pet count should be correct")
      .get("/pets")
      .expect(.ok)
      .expect(.json)
      .expect(Page<Pet.Get>.self) { content in
        XCTAssertEqual(content.metadata.total, total)
      }
      .test()
  }

  func testCreatePetFailsInvalidRace() throws {
    var newInvalidRacePet = exampleNewPet
    newInvalidRacePet.race = "succubus"

    try app
      .describe("Creating the new Pet fails validation")
      .post("/pets")
      .body(newInvalidRacePet)
      .expect(.badRequest)
      .expect(.json)
      .expect(ValidationError.self) { content in
        XCTAssertEqual(content.error, true)
        XCTAssertEqual(content.reason, "race is not dog or cat")
      }
      .test()
  }

  func testCreatePetFailsInvalidLifeSpan() throws {
    var newInvalidLifeSpanPet = exampleNewPet
    newInvalidLifeSpanPet.lifeSpan = .init(years: .init(minimum: -1, maximum: 1000))

    try app
      .describe("Creating the new Pet fails validation due to negative lifeSpan")
      .post("/pets")
      .body(newInvalidLifeSpanPet)
      .expect(.badRequest)
      .expect(.json)
      .expect(ValidationError.self) { content in
        XCTAssertEqual(content.error, true)
        XCTAssertEqual(
          content.reason,
          "lifeSpan years minimum is less than minimum of 0 and maximum is greater than maximum of 100"
        )
      }
      .test()
  }

  func testCreatePetFailsInvalidUrl() throws {
    var newInvalidLifeSpanPet = exampleNewPet
    newInvalidLifeSpanPet.urls = [
      .init(name: "wikipedia", url: "https")
    ]

    try app
      .describe("Creating the new Pet fails validation due to invalid url")
      .post("/pets")
      .body(newInvalidLifeSpanPet)
      .expect(.badRequest)
      .expect(.json)
      .expect(ValidationError.self) { content in
        XCTAssertEqual(content.error, true)
        XCTAssertEqual(content.reason, "urls at index 0 url is an invalid URL")
      }
      .test()
  }

  func testGetPet() throws {
    try app
      .describe("Get a seeded pet")
      .get("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, "dog")
        XCTAssertEqual(content.breed, "Staffordshire Bull Terrier")
      }
      .test()
  }

  func testGetPetNotFound() throws {
    try app
      .describe("Using an id not in database returns notFound")
      .get("/pets/A3234E16-C1C5-4887-8D76-94A22622690D")
      .expect(.notFound)
      .test()
  }

  func testDeletePet() throws {
    try app
      .describe("Deleting existing pet works")
      .delete("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.ok)
      .test()

    try app
      .describe("Trying to get the deleted pet results in notFound")
      .delete("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.notFound)
      .test()
  }

  func testDeletePetNotFound() throws {
    try app
      .describe("Deleting existing pet works")
      .delete("/pets/A3234E16-C1C5-4887-8D76-94A22622690D")
      .expect(.notFound)
      .test()
  }

  func testUpdatePetSucceeds() throws {
    var updatedPet = exampleNewPet
    updatedPet.urls = [
      .init(name: "url1", url: "https://www.url1.com"),
      .init(name: "url2", url: "https://www.url2.com"),
    ]

    try app
      .describe("Updating an existing pet works")
      .put("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .body(updatedPet)
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, updatedPet.race)
        XCTAssertEqual(content.urls, updatedPet.urls)
      }
      .test()

    try app
      .describe("Fetching the Pet later shows data was persisted")
      .get("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, updatedPet.race)
        XCTAssertEqual(content.urls, updatedPet.urls)
      }
      .test()
  }

  func testUpdatePetFailsInvalidUrl() throws {
    var updatedPet = exampleNewPet
    updatedPet.urls = [
      .init(name: "url1", url: "https")
    ]

    try app
      .describe("Trying to update with invalid urls fails")
      .put("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .body(updatedPet)
      .expect(.badRequest)
      .expect(.json)
      .expect(ValidationError.self) { content in
        XCTAssertEqual(content.error, true)
        XCTAssertEqual(content.reason, "urls at index 0 url is an invalid URL")
      }
      .test()
  }

  func testPatchPetSucceeds() throws {
    let petPatch = Pet.Patch(urls: [
      .init(name: "url1", url: "https://www.url1.com"),
      .init(name: "url2", url: "https://www.url2.com"),
    ])

    try app
      .describe("Updating an existing pet works")
      .patch("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .body(petPatch)
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, "dog")
        XCTAssertEqual(content.breed, "Staffordshire Bull Terrier")
        XCTAssertEqual(content.urls, petPatch.urls!)
      }
      .test()

    try app
      .describe("Fetching the Pet later shows data was persisted")
      .get("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, "dog")
        XCTAssertEqual(content.breed, "Staffordshire Bull Terrier")
        XCTAssertEqual(content.urls, petPatch.urls!)
      }
      .test()
  }

  func testPatchPetFailsInvalidUrl() throws {
    let petPatch = Pet.Patch(urls: [
      .init(name: "url1", url: "https"),
      .init(name: "url2", url: "https://www.url2.com"),
    ])

    try app
      .describe("Updating an existing pet fails with invalid url")
      .patch("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .body(petPatch)
      .expect(.badRequest)
      .expect(.json)
      .expect(ValidationError.self) { content in
        XCTAssertEqual(content.error, true)
        XCTAssertEqual(content.reason, "urls at index 0 url is an invalid URL")
      }
      .test()

    try app
      .describe("Fetching the Pet later shows data was not changed")
      .get("/pets/C63B7F5B-E628-4196-92B8-48DC93572DB1")
      .expect(.ok)
      .expect(.json)
      .expect(Pet.Get.self) { content in
        XCTAssertEqual(content.race, "dog")
        XCTAssertEqual(content.breed, "Staffordshire Bull Terrier")
        XCTAssertNotEqual(content.urls, petPatch.urls!)
      }
      .test()
  }

  func testListPets() throws {
    try app
      .describe("Pets should return ok")
      .get("/pets")
      .expect(.ok)
      .expect(.json)
      .expect(Page<Pet.Get>.self) { content in
        XCTAssertGreaterThan(content.items.count, 0)
      }
      .test()
  }

  let exampleNewPet = PetModel.CreateContent(
    race: "dog",
    breed: "Hot Dog",
    urls: [
      .init(
        name: "wikipedia",
        url: "https://en.wikipedia.org/wiki/Hot_dog"
      )
    ],
    weight: .init(
      pounds: .init(minimum: 10, maximum: 100), kilograms: .init(minimum: 10, maximum: 100)),
    height: .init(
      centimeters: .init(minimum: 10, maximum: 100), inches: .init(minimum: 10, maximum: 100)),
    lifeSpan: .init(years: .init(minimum: 10, maximum: 100))
  )
}

struct ValidationError: Content {
  let error: Bool
  let reason: String
}
