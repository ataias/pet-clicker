//
//  AppTestCase.swift
//
//
//  Created by Ataias Pereira Reis on 13/01/22.
//

import FluentPostgresDriver
import XCTVapor

@testable import App

open class AppTestCase: XCTestCase {
  var databaseName: String!
  var databaseId: DatabaseID { DatabaseID(string: databaseName) }
  var app: Application!

  open override func setUpWithError() throws {
    app = Application(.testing)
    try configure(app)

    // We may be running unit tests in parallel and to make sure they don't
    // interfere with each other we use separate postgres database instances.
    // The logic below will create a temporary database with a random name,
    // which is later cleaned up in the tearDown method.
    //
    // This could have been done with in-memory sqlite databases, however I
    // found that approach not reliable to really know if we have issues in our
    // database integration. For instance, in a relatively simple application I
    // managed to have a migration that worked in sqlite and didn't in
    // postgres, because sqlite by default is more permissive about types
    // (that's configurable though). To avoid other issues like that, using
    // postgres directly is better.
    //
    // The idea here is not original by the way: I recall dotnet core postgres
    // integration doing something similar for unit tests, but I think that's
    // something that I didn't have to configure, it was part of the library
    // being used.

    // Postgres has some issues with upper case database, so we lowercase it here
    databaseName = "pet_clicker_test_\(String.random(length: 10))".lowercased()

    let postgres = app.db(.psql) as! PostgresDatabase

    _ = try postgres.simpleQuery("CREATE DATABASE \(databaseName!)").wait()

    app.databases.use(
      .postgres(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: Environment.get("DATABASE_PORT").flatMap(Int.init(_:))
          ?? PostgresConfiguration.ianaPortNumber,
        username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
        password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
        database: databaseName
      ),
      as: databaseId
    )
    app.databases.default(to: databaseId)
    try app.autoMigrate().wait()

    try super.setUpWithError()
  }

  open override func tearDownWithError() throws {
    app.shutdown()

    // TODO: is there a "cleaner" way of "cleaning" this up? Using the same app
    // to destroy the database fails because of an open connection... and
    // shutting it down... well, also blocks us from actually dropping the
    // database later.
    let clearDatabaseApp = Application(.testing)
    try configure(clearDatabaseApp)
    let postgres = clearDatabaseApp.db(.psql) as! PostgresDatabase
    _ = try postgres.simpleQuery("DROP DATABASE \(databaseName!)").wait()
    clearDatabaseApp.shutdown()

    try super.tearDownWithError()
  }
}
