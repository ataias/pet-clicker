#!/usr/bin/env sh

set -e
set -x

export BUILD_IMAGE=ataias/swift:5.5.2-focal
export RUN_IMAGE=ataias/swift:5.5.2-focal-slim

time docker pull $BUILD_IMAGE
time docker pull $RUN_IMAGE

time docker build \
	--tag "$IMAGE" \
	--build-arg BUILD_IMAGE \
	--build-arg RUN_IMAGE \
	.

time docker push $IMAGE
