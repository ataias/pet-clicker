// swift-tools-version:5.5
import PackageDescription

let package = Package(
  name: "PetClickerBackend",
  platforms: [
    .macOS(.v12)
  ],
  dependencies: [
    // 💧 A server-side Swift web framework.
    .package(url: "https://github.com/vapor/vapor.git", from: "4.0.0"),
    .package(url: "https://github.com/vapor/fluent.git", from: "4.0.0"),
    .package(url: "https://github.com/vapor/fluent-postgres-driver.git", from: "2.2.2"),
    .package(url: "https://github.com/binarybirds/spec.git", from: "1.2.2"),
  ],
  targets: [
    .target(
      name: "Api",
      dependencies: [],
      swiftSettings: librarySwiftSettings
    ),
    .target(
      name: "App",
      dependencies: [
        .product(name: "Fluent", package: "fluent"),
        .product(name: "FluentPostgresDriver", package: "fluent-postgres-driver"),
        .product(name: "Vapor", package: "vapor"),
        .target(name: "Api"),
      ],
      swiftSettings: librarySwiftSettings
    ),
    .executableTarget(
      name: "Run",
      dependencies: [.target(name: "App")],
      swiftSettings: [
        .unsafeFlags(["-parse-as-library"])
      ]
    ),
    .testTarget(
      name: "AppTests",
      dependencies: [
        .target(name: "App"),
        .product(name: "XCTVapor", package: "vapor"),
        .product(name: "Spec", package: "spec"),
      ]),
  ]
)

let librarySwiftSettings: [SwiftSetting] = [
  // Enable better optimizations when building in Release configuration. Despite the use of
  // the `.unsafeFlags` construct required by SwiftPM, this flag is recommended for Release
  // builds. See <https://github.com/swift-server/guides/blob/main/docs/building.md#building-for-production> for details.
  .unsafeFlags(["-cross-module-optimization"], .when(configuration: .release))
]
