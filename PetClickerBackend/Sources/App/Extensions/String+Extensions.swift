//
//  String+Extensions.swift
//
//
//  Created by Ataias Pereira Reis on 13/01/22.
//

import Foundation

extension String {
  /// Generates a random string with given length
  ///
  /// - Source: [StackOverflow](https://stackoverflow.com/a/26845710)
  /// - License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
  static func random(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0..<length).map { _ in letters.randomElement()! })
  }
}
