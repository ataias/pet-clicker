//
//  PetController.swift
//
//
//  Created by Ataias Pereira Reis on 16/01/22.
//

import Api
import Fluent
import Vapor

struct PetController: RouteCollection {
  func boot(routes: RoutesBuilder) throws {
    let pets = routes.grouped("pets")
    pets.get(use: list)
    pets.post(use: create)
    pets.group(":petID") { pet in
      pet.get(use: get)
      pet.put(use: update)
      pet.patch(use: patch)
      pet.delete(use: delete)
    }
  }

  func get(req: Request) async throws -> Pet.Get {
    guard let idStr = req.parameters.get("petID"), let id = UUID(uuidString: idStr) else {
      throw Abort(.badRequest)
    }
    guard let model = try await PetModel.find(id, on: req.db) else {
      throw Abort(.notFound)
    }
    return model.getContent
  }

  func list(req: Request) async throws -> Page<Pet.Get> {
    return try await PetModel.query(on: req.db).paginate(for: req).map(\.listContent)
  }

  func create(req: Request) async throws -> Response {
    try Pet.Upsert.validate(content: req)
    let createObject = try req.content.decode(Pet.Upsert.self)
    let model = PetModel()
    try model.create(createObject)
    try await model.save(on: req.db)
    return try await model.getContent.encodeResponse(status: .created, for: req)
  }

  func update(req: Request) async throws -> Pet.Get {
    try Pet.Upsert.validate(content: req)
    let updateObject = try req.content.decode(Pet.Upsert.self)

    guard let idStr = req.parameters.get("petID"), let id = UUID(uuidString: idStr) else {
      throw Abort(.badRequest)
    }
    guard let model = try await PetModel.find(id, on: req.db) else {
      throw Abort(.notFound)
    }

    try model.update(updateObject)
    try await model.save(on: req.db)
    return model.getContent
  }

  func patch(req: Request) async throws -> Pet.Get {
    try Pet.Patch.validate(content: req)
    let patchObject = try req.content.decode(Pet.Patch.self)

    guard let idStr = req.parameters.get("petID"), let id = UUID(uuidString: idStr) else {
      throw Abort(.badRequest)
    }
    guard let model = try await PetModel.find(id, on: req.db) else {
      throw Abort(.notFound)
    }

    try model.patch(patchObject)
    try await model.save(on: req.db)
    return model.getContent
  }

  func delete(req: Request) async throws -> HTTPStatus {
    guard let pet = try await PetModel.find(req.parameters.get("petID"), on: req.db) else {
      throw Abort(.notFound)
    }
    try await pet.delete(on: req.db)
    return .ok
  }
}
