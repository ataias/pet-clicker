//
//  ImageController.swift
//
//
//  Created by Ataias Pereira Reis on 16/01/22.
//

import Api
import Fluent
import Vapor

struct ImageController: RouteCollection {
  func boot(routes: RoutesBuilder) throws {
    let images = routes.grouped("images")
    images.get(use: index)
    images.post(use: create)
    images.group(":imageID") { image in
      image.delete(use: delete)
    }
  }

  func index(req: Request) async throws -> [Image.Get] {
    let images = try await ImageModel.query(on: req.db).with(\.$pets).all()
    return images.map(Image.Get.from(model:))
  }

  func create(req: Request) async throws -> Response {
    let model = try req.content.decode(ImageModel.self)
    try await model.save(on: req.db)
    let image = Image.Get.from(model: model)
    return try await image.encodeResponse(status: .created, for: req)
  }

  // TODO: use DTO here
  func update(req: Request) async throws -> ImageModel {
    let updatedImage = try req.content.decode(ImageModel.self)
    guard
      let imageId = updatedImage.id,
      try await ImageModel.find(imageId, on: req.db) != nil
    else {
      throw Abort(.notFound)
    }
    try await updatedImage.save(on: req.db)
    return updatedImage
  }

  func delete(req: Request) async throws -> HTTPStatus {
    guard let image = try await ImageModel.find(req.parameters.get("imageID"), on: req.db) else {
      throw Abort(.notFound)
    }
    try await image.delete(on: req.db)
    return .ok
  }
}

// MARK: DTO Converters
extension Image.Get: Content {
  static func from(model: ImageModel) -> Image.Get {
    return .init(
      id: model.id!,
      description: model.description,
      assets: model.assets.map(Image.Asset.from(model:)),
      clicks: model.clicks,
      petIds: model.pets.compactMap { petModel in
        petModel.id
      }
    )
  }
}

extension Image.Asset {
  static func from(model: ImageModel.Asset) -> Image.Asset {
    return .init(
      id: model.id,
      height: model.height,
      width: model.width,
      extension: .init(rawValue: model.extension.rawValue)!,
      url: model.url
    )
  }
}
