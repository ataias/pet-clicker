//
//  GetContentRepresentable.swift
//
//
//  Created by Ataias Pereira Reis on 30/12/21.
//

import Vapor

protocol GetContentRepresentable {
  associatedtype GetContent: Content

  var getContent: GetContent { get }
}
