//
//  UpdateContentRepresentable.swift
//
//
//  Created by Ataias Pereira Reis on 30/12/21.
//

import Vapor

protocol UpdateContentRepresentable: GetContentRepresentable {
  associatedtype UpdateContent: ValidatableContent
  func update(_: UpdateContent) throws
}
