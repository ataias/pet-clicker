//
//  ValidatableContent.swift
//
//
//  Created by Ataias Pereira Reis on 30/12/21.
//

import Vapor

protocol ValidatableContent: Content, Validatable {}
