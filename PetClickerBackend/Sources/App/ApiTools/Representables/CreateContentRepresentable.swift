//
//  CreateContentRepresentable.swift
//
//
//  Created by Ataias Pereira Reis on 30/12/21.
//

import Vapor

protocol CreateContentRepresentable: GetContentRepresentable {
  associatedtype CreateContent: ValidatableContent

  /// Updates the model based on the CreateContent type
  func create(_: CreateContent) throws
}
