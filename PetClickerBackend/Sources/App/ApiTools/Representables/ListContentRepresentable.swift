//
//  ListContentRepresentable.swift
//
//
//  Created by Ataias Pereira Reis on 27/12/21.
//

import Vapor

protocol ListContentRepresentable {
  associatedtype ListItem: Content

  var listContent: ListItem { get }
}
