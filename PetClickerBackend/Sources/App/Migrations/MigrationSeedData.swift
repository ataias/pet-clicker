//
//  MigrationSeedData.swift
//
//
//  Created by Ataias Pereira Reis on 16/01/22.
//

import Fluent
import Vapor

// MARK: Main
struct MigrationSeedData: AsyncMigration {
  func prepare(on database: Database) async throws {
    try await database.transaction { database in
      try await Seed.Images.generic(on: database)
      try await Seed.Dog.bullTerrier(on: database)
      try await Seed.Dog.shibaInu(on: database)
      try await Seed.Dog.siberianHusky(on: database)
    }
  }

  func revert(on database: Database) async throws {
    try await database.transaction { database in
      try await ImageModel.query(on: database).delete()
      try await PetModel.query(on: database).delete()
    }
  }
}

// MARK: By Category
private enum Seed {
  enum Images {
    static func generic(on database: Database) async throws {
      let images = (1...100).map { i in
        ImageModel(
          description: "A random image",
          assets: [
            .init(
              id: UUID(),
              height: 10,
              width: 10,
              extension: .jpeg,
              url: "http://url1.com/\(i).jpeg"
            )
          ]
        )
      }
      try await images.create(on: database)
    }
  }
  enum Dog {
    static func bullTerrier(on database: Database) async throws {
      let dog = PetModel(
        id: UUID(uuidString: "C63B7F5B-E628-4196-92B8-48DC93572DB1")!,
        race: .dog,
        breed: "Staffordshire Bull Terrier",
        urls: [
          .init(
            name: "wikipedia",
            url: URL(string: "https://en.wikipedia.org/wiki/Staffordshire_Bull_Terrier")!
          )
        ],
        weight: PetModel.Weight(pounds: 24...37, kilograms: 11...17),
        height: PetModel.Height(centimeters: 36...41, inches: 14...16),
        lifeSpan: PetModel.LifeSpan(years: 12...15)
      )
      try await dog.create(on: database)

      let images = [
        ImageModel(
          description: "A white bull terrier",
          assets: [
            .init(
              id: UUID(),
              height: 853,
              width: 1280,
              extension: .jpeg,
              url:
                "https://cdn.pixabay.com/photo/2017/09/14/11/47/staffordshire-bull-terrier-2748733_1280.jpg"
            )
          ]
        ),
        ImageModel(
          description: "A black bull terrier",
          assets: [
            .init(
              id: UUID(),
              height: 900,
              width: 600,
              extension: .jpeg,
              url:
                "https://upload.wikimedia.org/wikipedia/commons/c/ce/GLIMMER_MAN_Domidar_Dogs.jpg"
            )
          ]
        ),
      ]
      try await images.create(on: database)

      let petImages = try images.map { image in
        try PetImageModel(pet: dog, image: image)
      }
      try await petImages.create(on: database)
    }
    static func shibaInu(on database: Database) async throws {
      let dog = PetModel(
        id: UUID(uuidString: "39D20EE8-6DC8-47A9-9BA6-4AA30DA02F68")!,
        race: .dog,
        breed: "Shiba Inu",
        urls: [
          .init(
            name: "wikipedia",
            url: URL(string: "https://en.wikipedia.org/wiki/Shiba_Inu")!
          )
        ],
        weight: PetModel.Weight(pounds: 18...22, kilograms: 8...10),
        height: PetModel.Height(centimeters: 33...43, inches: 13...17),
        lifeSpan: PetModel.LifeSpan(years: 13...15)
      )
      try await dog.save(on: database)

      let images = [
        ImageModel(
          description: "A caramel Shiba Inu",
          assets: [
            .init(
              id: UUID(),
              height: 980,
              width: 1280,
              extension: .jpeg,
              url:
                "https://upload.wikimedia.org/wikipedia/commons/6/6b/Taka_Shiba.jpg"
            )
          ]
        ),
        ImageModel(
          description: "A cream Shiba Inu",
          assets: [
            .init(
              id: UUID(),
              height: 1370,
              width: 2048,
              extension: .jpeg,
              url:
                "https://upload.wikimedia.org/wikipedia/commons/1/10/Shiba_Inu_cream.jpg"
            )
          ]
        ),
      ]
      try await images.create(on: database)

      let petImages = try images.map { image in
        try PetImageModel(pet: dog, image: image)
      }
      try await petImages.create(on: database)
    }
    static func siberianHusky(on database: Database) async throws {
      let dog = PetModel(
        id: UUID(uuidString: "F26FCB81-50DC-44B5-9211-6F60FDB652E7")!,
        race: .dog,
        breed: "Siberian Husky",
        urls: [
          .init(
            name: "wikipedia",
            url: URL(string: "https://en.wikipedia.org/wiki/Siberian_Husky")!
          )
        ],
        weight: PetModel.Weight(pounds: 35...60, kilograms: 16...27),
        height: PetModel.Height(centimeters: 51...60, inches: 20...23.5),
        lifeSpan: PetModel.LifeSpan(years: 12...14)
      )
      try await dog.save(on: database)

      let images = [
        ImageModel(
          description: "A Siberian Husky in the snow",
          assets: [
            .init(
              id: UUID(),
              height: 1370,
              width: 2048,
              extension: .jpeg,
              url:
                "https://upload.wikimedia.org/wikipedia/commons/3/33/Siberian_Husky_sable.jpg"
            )
          ]
        ),
        ImageModel(
          description: "A Siberian Husky with eyes of different colors",
          assets: [
            .init(
              id: UUID(),
              height: 600,
              width: 800,
              extension: .jpeg,
              url:
                "https://upload.wikimedia.org/wikipedia/commons/f/f5/Siberian_Husky_copper_bi-eye.jpg"
            )
          ]
        ),
      ]
      try await images.create(on: database)

      let petImages = try images.map { image in
        try PetImageModel(pet: dog, image: image)
      }
      try await petImages.create(on: database)
    }
  }
}
