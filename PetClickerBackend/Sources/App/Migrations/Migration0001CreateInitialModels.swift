//
//  Migration0001CreateInitialModels.swift
//
//
//  Created by Ataias Pereira Reis on 15/01/22.
//

import Fluent
import Vapor

struct Migration0001CreateInitialModels: AsyncMigration {
  func prepare(on database: Database) async throws {
    try await database.transaction { database in
      let animalType = try await database.enum("animal_type")
        .case("cat")
        .case("dog")
        .create()

      try await database.schema("pets")
        .id()
        .field("race", animalType, .required)
        .field("breed", .string, .required)
        .unique(on: "race", "breed")
        .field("urls", .array(of: .json), .required)
        .field("weight", .json)
        .field("height", .json)
        .field("life_span", .json, .required)
        .field("created_at", .datetime)
        .field("updated_at", .datetime)
        .field("deleted_at", .datetime)
        .create()

      try await database.schema("images")
        .id()
        .field("assets", .array(of: .json), .required)
        .field("description", .string, .required)
        .field("clicks", .int64, .required)
        .field("created_at", .datetime)
        .field("updated_at", .datetime)
        .field("deleted_at", .datetime)
        .create()

      try await database.schema("pet+image")
        .id()
        .field("pet_id", .uuid, .required, .references("pets", "id"))
        .field("image_id", .uuid, .required, .references("images", "id"))
        .unique(on: "pet_id", "image_id")
        .field("created_at", .datetime)
        .field("updated_at", .datetime)
        .field("deleted_at", .datetime)
        .create()
    }
  }

  func revert(on database: Database) async throws {
    try await database.transaction { database in
      try await database.schema("pet+image").delete()
      try await database.schema("images").delete()
      try await database.schema("pets").delete()
      try await database.enum("animal_type").delete()
    }
  }
}
