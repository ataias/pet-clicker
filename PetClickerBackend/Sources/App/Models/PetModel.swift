//
//  PetModel.swift
//
//
//  Created by Ataias Pereira Reis on 12/12/21.
//

import Fluent
import Vapor

final class PetModel: Model, Content {
  static let schema = "pets"

  // MARK: Fields
  @ID(key: .id)
  var id: UUID?

  @Enum(key: "race")
  var race: Animal

  @Field(key: "breed")
  var breed: String

  @Field(key: "urls")
  var urls: [NamedURL]

  @Field(key: "weight")
  var weight: Weight

  @Field(key: "height")
  var height: Height

  @Field(key: "life_span")
  var lifeSpan: LifeSpan

  @Siblings(through: PetImageModel.self, from: \.$pet, to: \.$image)
  var images: [ImageModel]

  // MARK: Metadata
  @Timestamp(key: "created_at", on: .create)
  var createdAt: Date?

  @Timestamp(key: "updated_at", on: .update)
  var updatedAt: Date?

  @Timestamp(key: "deleted_at", on: .delete)
  var deletedAt: Date?

  // MARK: Constructors
  init() {}

  init(
    id: UUID? = nil,
    race: Animal,
    breed: String,
    urls: [NamedURL] = [],
    weight: Weight,
    height: Height,
    lifeSpan: LifeSpan
  ) {
    self.id = id
    self.race = race
    self.breed = breed
    self.urls = urls
    self.weight = weight
    self.height = height
    self.lifeSpan = lifeSpan
  }
}

// MARK: Auxiliary Enums

extension PetModel {
  // TODO: I could just save a single one, even if I want to send multiple in the api... those are distinct
  struct Weight: Codable {
    var pounds: ClosedRange<Double>
    var kilograms: ClosedRange<Double>
  }

  struct Height: Codable {
    var centimeters: ClosedRange<Double>
    var inches: ClosedRange<Double>
  }

  struct LifeSpan: Codable {
    var years: ClosedRange<Double>
  }

  enum Animal: String, Codable, CaseIterable {
    case dog
    case cat
  }

  struct NamedURL: Codable {
    var name: String
    var url: URL
  }
}
