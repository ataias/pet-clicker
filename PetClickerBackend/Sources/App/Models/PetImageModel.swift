//
//  PetImageModel.swift
//
//
//  Created by Ataias Pereira Reis on 13/12/21.
//

import Fluent
import Vapor

/// Pivot table to create an N-N relationship between the Pet and Image tables.
final class PetImageModel: Model {
  static let schema = "pet+image"

  // MARK: Fields
  @ID(key: .id)
  var id: UUID?

  @Parent(key: "pet_id")
  var pet: PetModel

  @Parent(key: "image_id")
  var image: ImageModel

  // MARK: Metadata
  @Timestamp(key: "created_at", on: .create)
  var createdAt: Date?

  @Timestamp(key: "updated_at", on: .update)
  var updatedAt: Date?

  @Timestamp(key: "deleted_at", on: .delete)
  var deletedAt: Date?

  // MARK: Constructors
  init() {}

  init(id: UUID? = nil, pet: PetModel, image: ImageModel) throws {
    self.id = id
    self.$pet.id = try pet.requireID()
    self.$image.id = try image.requireID()
  }
}
