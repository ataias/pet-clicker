//
//  ImageModel.swift
//
//
//  Created by Ataias Pereira Reis on 12/12/21.
//

import Fluent
import Vapor

final class ImageModel: Model, Content {
  static let schema = "images"

  // MARK: Fields
  @ID(key: .id)
  var id: UUID?

  @Field(key: "description")
  var description: String

  @Field(key: "assets")
  var assets: [Asset]

  @Field(key: "clicks")
  var clicks: Int64

  @Siblings(through: PetImageModel.self, from: \.$image, to: \.$pet)
  var pets: [PetModel]

  // MARK: Metadata
  @Timestamp(key: "created_at", on: .create)
  var createdAt: Date?

  @Timestamp(key: "updated_at", on: .update)
  var updatedAt: Date?

  @Timestamp(key: "deleted_at", on: .delete)
  var deletedAt: Date?

  // MARK: Constructors
  init() {}

  init(
    id: UUID? = nil,
    description: String,
    assets: [Asset],
    clicks: Int64 = 0
  ) {
    self.id = id
    self.description = description
    self.assets = assets
    self.clicks = clicks
  }
}

// MARK: Image File

extension ImageModel {
  /// An image asset points to an image of a specific height, width and extension
  struct Asset: Codable {
    var id: UUID
    var height: Int
    var width: Int
    /// The extension of the image file
    var `extension`: FileExtension
    var url: String
  }

  enum FileExtension: String, Codable {
    case webp
    case png
    case jpeg
  }
}

// MARK: Data Transfer Objects
extension ImageModel {
  struct Post: Decodable {
    var url: String
    var width: Int
    var height: Int
    var clicks: Int64
  }
}
