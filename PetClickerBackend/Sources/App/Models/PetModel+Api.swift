//
//  PetModel+Api.swift
//
//
//  Created by Ataias Pereira Reis on 19/01/22.
//

import Api
import Vapor

extension PetModel: ApiRepresentable {
  var getContent: Pet.Get {
    .init(
      // TODO: use try requireId() to avoid crashing application if id is missing and log an error instead
      id: id!,
      race: race.rawValue,
      breed: breed,
      urls: urls.map(Pet.NamedURL.from),
      weight: Pet.Weight.from(model: weight),
      height: Pet.Height.from(model: height),
      lifeSpan: Pet.LifeSpan.from(model: lifeSpan)
    )
  }

  var listContent: Pet.Get {
    getContent
  }

  func create(_ object: Pet.Upsert) throws {
    try upsert(object)
  }

  func update(_ object: Pet.Upsert) throws {
    try upsert(object)
  }

  func patch(_ object: Pet.Patch) throws {
    if let objectRace = object.race, let race = Animal(rawValue: objectRace) {
      self.race = race
    }
    self.breed = object.breed ?? breed
    if let urls = object.urls {
      self.urls = try urls.map {
        guard let url = URL(string: $0.url) else {
          throw Abort(.badRequest)
        }
        return PetModel.NamedURL(name: $0.name, url: url)
      }
    }
    if let weight = object.weight {
      self.weight = PetModel.Weight(weight)
    }
    if let height = object.height {
      self.height = PetModel.Height(height)
    }
    if let lifeSpan = object.lifeSpan {
      self.lifeSpan = PetModel.LifeSpan(lifeSpan)
    }
  }

  private func upsert(_ object: Pet.Upsert) throws {
    self.race = Animal(rawValue: object.race)!
    self.breed = object.breed
    self.urls = try object.urls.map {
      guard let url = URL(string: $0.url) else {
        throw Abort(.badRequest)
      }
      return PetModel.NamedURL(name: $0.name, url: url)
    }
    self.weight = PetModel.Weight(object.weight)
    self.height = PetModel.Height(object.height)
    self.lifeSpan = PetModel.LifeSpan(object.lifeSpan)
  }
}

// MARK: DTO extensions
extension Pet.Get: Content {}
extension Pet.Upsert: ValidatableContent {
  public static func validations(_ validations: inout Validations) {
    validations.add("race", as: String.self, is: .in(PetModel.Animal.allCases.map(\.rawValue)))
    validations.add("lifeSpan", required: true) { validations in
      validations.add("years", required: true) { validations in
        validations.add("minimum", as: Int.self, is: .range(0...100))
        validations.add("maximum", as: Int.self, is: .range(0...100))
      }
    }
    validations.add(each: "urls", required: true) { _, namedURL in
      namedURL.add("url", as: String.self, is: .url, required: true)
    }
  }
}
extension Pet.Patch: ValidatableContent {
  public static func validations(_ validations: inout Validations) {
    validations.add(
      "race", as: String.self, is: .in(PetModel.Animal.allCases.map(\.rawValue)), required: false)
    validations.add("lifeSpan", required: false) { validations in
      validations.add("years", required: true) { validations in
        validations.add("minimum", as: Int.self, is: .range(0...100))
        validations.add("maximum", as: Int.self, is: .range(0...100))
      }
    }
    validations.add(each: "urls", required: false) { _, namedURL in
      namedURL.add("url", as: String.self, is: .url, required: true)
    }
  }
}

extension Pet.Weight {
  static func from(model: PetModel.Weight) -> Pet.Weight {
    return .init(
      pounds: Pet.Range(minimum: model.pounds.lowerBound, maximum: model.pounds.upperBound),
      kilograms: Pet.Range(minimum: model.kilograms.lowerBound, maximum: model.kilograms.upperBound)
    )
  }
}

extension Pet.Height {
  static func from(model: PetModel.Height) -> Pet.Height {
    return .init(centimeters: .init(model.centimeters), inches: .init(model.inches))
  }
}

extension Pet.LifeSpan {
  static func from(model: PetModel.LifeSpan) -> Pet.LifeSpan {
    return .init(years: .init(model.years))
  }
}

extension Pet.NamedURL {
  static func from(model: PetModel.NamedURL) -> Pet.NamedURL {
    return .init(name: model.name, url: model.url.absoluteString)
  }
}

// MARK: Model Extensions

extension PetModel.Weight {
  init(_ from: Pet.Weight) {
    self.pounds = from.pounds.minimum...from.pounds.maximum
    self.kilograms = from.kilograms.minimum...from.kilograms.maximum
  }
}

extension PetModel.Height {
  init(_ from: Pet.Height) {
    self.inches = from.inches.minimum...from.inches.maximum
    self.centimeters = from.centimeters.minimum...from.centimeters.maximum
  }
}

extension PetModel.LifeSpan {
  init(_ from: Pet.LifeSpan) {
    self.years = from.years.minimum...from.years.maximum
  }
}
