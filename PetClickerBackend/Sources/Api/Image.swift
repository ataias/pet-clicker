//
//  Image.swift
//
//
//  Created by Ataias Pereira Reis on 17/01/22.
//

import Foundation

public enum Image {
  public struct Get: Codable {
    public let id: UUID
    public let description: String
    public let assets: [Image.Asset]
    public let clicks: Int64
    /// The ids of pets that appear in the image
    public let petIds: [UUID]

    public init(
      id: UUID,
      description: String,
      assets: [Image.Asset],
      clicks: Int64,
      petIds: [UUID]
    ) {
      self.id = id
      self.description = description
      self.assets = assets
      self.clicks = clicks
      self.petIds = petIds
    }
  }

  /// Update an image object
  ///
  /// It is noticeable that ``Get`` does not contain a field like ``Update/image``, but it contains instead ``Get/assets``, which is even of another type. The reason is that images are processed in the backend to have better representations, but in the end they are the same image, so an update to an image means updating a single image, that is then processed and can be fetched as a list of images in different resolutions and/or formats.
  public struct Update: Codable {
    public let description: String
    public let image: Data
    public let clicks: Int64
    /// The ids of pets that appear in the image
    public let petIds: [UUID]

    public init(
      description: String,
      image: Data,
      clicks: Int64,
      petIds: [UUID]
    ) {
      self.description = description
      self.image = image
      self.clicks = clicks
      self.petIds = petIds
    }
  }

  /// Patch an image object
  ///
  /// It is noticeable that ``Get`` does not contain a field like ``Patch/image``, but it contains instead ``Get/assets``, which is even of another type. The reason is that images are processed in the backend to have better representations, but in the end they are the same image, so an update to an image means updating a single image, that is then processed and can be fetched as a list of images in different resolutions and/or formats.
  public struct Patch: Codable {
    public let description: String?
    public let image: Data?
    public let clicks: Int64?
    /// The ids of pets that appear in the image
    public let petIds: [UUID]?

    public init(
      description: String?,
      image: Data?,
      clicks: Int64?,
      petIds: [UUID]?
    ) {
      self.description = description
      self.image = image
      self.clicks = clicks
      self.petIds = petIds
    }
  }
}

extension Image {
  public struct Asset: Codable {
    public var id: UUID
    public var height: Int
    public var width: Int
    /// The extension of the image file
    public var `extension`: FileExtension
    public var url: String

    public init(
      id: UUID,
      height: Int,
      width: Int,
      extension: FileExtension,
      url: String
    ) {
      self.id = id
      self.height = height
      self.width = width
      self.`extension` = `extension`
      self.url = url
    }
  }

  public enum FileExtension: String, Codable {
    case webp
    case png
    case jpeg
  }
}
