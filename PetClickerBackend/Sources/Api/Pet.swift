//
//  Pet.swift
//
//
//  Created by Ataias Pereira Reis on 17/01/22.
//

import Foundation

// MARK: Pet
public enum Pet {
  public struct Get: Codable {
    public let id: UUID
    public let race: String
    public let breed: String
    public let urls: [NamedURL]
    public let weight: Pet.Weight
    public let height: Pet.Height
    public let lifeSpan: Pet.LifeSpan

    public init(
      id: UUID,
      race: String,
      breed: String,
      urls: [NamedURL],
      weight: Pet.Weight,
      height: Pet.Height,
      lifeSpan: Pet.LifeSpan
    ) {
      self.id = id
      self.race = race
      self.breed = breed
      self.urls = urls
      self.weight = weight
      self.height = height
      self.lifeSpan = lifeSpan
    }
  }

  /// DTO for updating or inserting
  ///
  /// This DTO does not contain the id because the id will come from the route
  public struct Upsert: Codable {
    public var race: String
    public var breed: String
    public var urls: [NamedURL]
    public var weight: Pet.Weight
    public var height: Pet.Height
    public var lifeSpan: Pet.LifeSpan

    public init(
      race: String,
      breed: String,
      urls: [NamedURL],
      weight: Pet.Weight,
      height: Pet.Height,
      lifeSpan: Pet.LifeSpan
    ) {
      self.race = race
      self.breed = breed
      self.urls = urls
      self.weight = weight
      self.height = height
      self.lifeSpan = lifeSpan
    }
  }

  public struct Patch: Codable {
    public var race: String?
    public var breed: String?
    public var urls: [NamedURL]?
    public var weight: Pet.Weight?
    public var height: Pet.Height?
    public var lifeSpan: Pet.LifeSpan?

    public init(
      race: String? = nil,
      breed: String? = nil,
      urls: [NamedURL]? = nil,
      weight: Pet.Weight? = nil,
      height: Pet.Height? = nil,
      lifeSpan: Pet.LifeSpan? = nil
    ) {
      self.race = race
      self.breed = breed
      self.urls = urls
      self.weight = weight
      self.height = height
      self.lifeSpan = lifeSpan
    }
  }
}

// MARK: Auxiliary

extension Pet {
  public struct Weight: Codable, Equatable {
    public var pounds: Pet.Range
    public var kilograms: Pet.Range

    public init(pounds: Pet.Range, kilograms: Pet.Range) {
      self.pounds = pounds
      self.kilograms = kilograms
    }
  }

  public struct Height: Codable, Equatable {
    public var centimeters: Pet.Range
    public var inches: Pet.Range

    public init(centimeters: Pet.Range, inches: Pet.Range) {
      self.centimeters = centimeters
      self.inches = inches
    }
  }

  public struct LifeSpan: Codable, Equatable {
    public var years: Pet.Range

    public init(years: Pet.Range) {
      self.years = years
    }
  }

  /// A numeric range
  public struct Range: Codable, Equatable {
    public var minimum: Double
    public var maximum: Double

    public init(minimum: Double, maximum: Double) {
      self.minimum = minimum
      self.maximum = maximum
    }

    public init(_ range: ClosedRange<Double>) {
      self.minimum = range.lowerBound
      self.maximum = range.upperBound
    }
  }

  public struct NamedURL: Codable, Equatable {
    public var name: String
    public var url: String

    public init(name: String, url: String) {
      self.name = name
      self.url = url
    }
  }
}
