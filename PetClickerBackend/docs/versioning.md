# Versioning

Versioning PetClickerBackend should follow semantic versioning. The version should
be set via a git tag on a commit and then pushed. Tags should be annotated
(`git tag -a`) and should include a summary of main changes (a changelog).

Tags should be created on the main branch, as anything on merge requests may be
rebased and lost, but such a thing is avoided for the main branch.

## Helpful commands

### Create a tag

```sh
git tag -a v0.0.1
```


### Push tags

```sh
git push --tags
```
